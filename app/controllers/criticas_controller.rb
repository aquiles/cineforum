class CriticasController < ApplicationController
    before_action :set_postagem

    def create
        @postagem.criticas.create! criticas_params
        redirect_to @postagem
    end


    private
    def set_postagem
        @postagem = Postagem.find(params[:postagem_id])
    end

    def criticas_params
        params.required(:critica).permit(:post)
    end
end
