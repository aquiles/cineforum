class Postagem < ApplicationRecord
    has_many :criticas, :dependent => :destroy
    validates_presence_of :titulo, :post
end
