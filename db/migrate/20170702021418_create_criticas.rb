class CreateCriticas < ActiveRecord::Migration[5.1]
  def change
    create_table :criticas do |t|
      t.references :postagem, foreign_key: true
      t.text :post

      t.timestamps
    end
  end
end
